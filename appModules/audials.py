#appModules/audials.py
#A part of NonVisual Desktop Access (NVDA)
#Copyright (C) 2006-2012 NVDA Contributors
#This file is covered by the GNU General Public License.
#See the file COPYING for more details.
import appModuleHandler
import controlTypes
from NVDAObjects.IAccessible import sysListView32

class audialslistitem(sysListView32.ListItem):
	def _get_name(self):
		return self.displayText
class AppModule(appModuleHandler.AppModule):
	def chooseNVDAObjectOverlayClasses(self, obj, clslist):
		if obj.role == controlTypes.ROLE_LISTITEM:
			clslist.insert(0, audialslistitem)
